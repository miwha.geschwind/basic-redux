import { createStore } from 'redux'

const initialState = {
  name: {
    firstName: 'bob',
    lastName: 'smith',
  },
  age: 49,
  hobbies: [
    {name: 'rafting', frequency: 'often'},
    {name: 'baking', frequency: 'sometimes'},
    {name: 'watching TV', frequency: 'often'},
    {name: 'taking naps', frequency: 'almost never'},
  ],
};

console.log('initial state: ', initialState, '\n');

/**
 * This is a reducer, a pure function with (state, action) => state signature.
 * It describes how an action transforms the state into the next state.
 *
 * The shape of the state is up to you: it can be a primitive, an array, an object,
 * or even an Immutable.js data structure. The only important part is that you should
 * not mutate the state object, but return a new object if the state changes.
 *
 * In this example, we use a `switch` statement and strings, but you can use a helper that
 * follows a different convention (such as function maps) if it makes sense for your
 * project.
 */

// the result of a reducer function is the new state
function counter(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_NAME':
      // don't ever do an assignment into state (this is called mutating state, and is very bad)
      // state.name.firstName = action.name;
      // return state

      // damn... overwrote the WHOLE name object...
      // return Object.assign({}, state, { name: { fistName: action.fistName } });

      // do this!
      return Object.assign(
        {},
        state,
        { name: Object.assign({}, state.name, { firstName: action.fistName }) });
    default:
      return state
  }
}

// Create a Redux store holding the state of your app.
// Its API is { subscribe, dispatch, getState }.
let store = createStore(counter);

// You can use subscribe() to update the UI in response to state changes.
// Normally you'd use a view binding library (e.g. React Redux) rather than subscribe() directly.
// However it can also be handy to persist the current state in the localStorage.

store.subscribe(() => console.log('new state: ', store.getState()));

// The only way to mutate the internal state is to dispatch an action.
// The actions can be serialized, logged or stored and later replayed.

store.dispatch({ type: 'CHANGE_NAME', fistName: 'sally' }); // dispatching an action - an action is just an object

